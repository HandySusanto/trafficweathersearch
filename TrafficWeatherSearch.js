//TODO: OneMapToken will expire. Need backend to renew and provide latest token.
var OneMapToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjcwNjIsInVzZXJfaWQiOjcwNjIsImVtYWlsIjoiaGFuZHlfc3VzYW50b0BuZWEuZ292LnNnIiwiZm9yZXZlciI6ZmFsc2UsImlzcyI6Imh0dHA6XC9cL29tMi5kZmUub25lbWFwLnNnXC9hcGlcL3YyXC91c2VyXC9zZXNzaW9uIiwiaWF0IjoxNjEyMTY1NzYzLCJleHAiOjE2MTI1OTc3NjMsIm5iZiI6MTYxMjE2NTc2MywianRpIjoiYTczNWRhZDdhMmFjN2EwODJiMGZlODRiNjUzM2UwMjUifQ.o7_h4U1yj3Nk4tk6UlLuf1idIyU-8MI0L2KAX9wSTwo";

//global vars
var searchDate, searchTime;
var jsonWeatherForecast;


//Search Button action
function searchTraffic()
{  
  var instanceDate = M.Datepicker.getInstance(document.querySelector("#datepicker"));
  var APITraffic = "https://api.data.gov.sg/v1/transport/traffic-images";
  var APIWeatherForecast = "https://api.data.gov.sg/v1/environment/2-hour-weather-forecast";
  var res;
  var friendlyDT = convertFriendlyDateTimeObj(new Date());
  
  //Initialisation
  document.getElementById("panelLocationList").innerHTML = "";
  document.getElementById("panelScreenshot").innerHTML = "";
  document.getElementById("panelWeatherForecast").innerHTML = "";  
  searchDate = instanceDate.toString();
  searchTime = $('.timepicker').val();
  if (searchDate == "")
  {
	  searchDate = friendlyDT.friendlyDate;
  }
  if (searchTime == "")
  {
	  searchTime = friendlyDT.friendlyTime;
  }
  
  //Invoke Traffic API and then populate the LocationList
  loadProgressBar();
  axios.get(APITraffic, {
	params: {
	  date_time: searchDate + "T" + searchTime + ":00"
	}
  })
  .then((response) => {
	populateLocationList(response);	  
  })
  .catch(error => {
    console.log(error)
  });
  
  //Invoke Weather API and then store it to jsonWeatherForecast
  loadProgressBar();
  axios.get(APIWeatherForecast, {
	params:{
	  date_time: searchDate + "T" + searchTime + ":00"
	}
  })	
  .then((response) => {	  
    jsonWeatherForecast = response;		  
  })
  .catch(error => {
	console.log(error)
  });	  
}
	  

//Populate Location List based on Traffic CAM API
function populateLocationList(res)
{
	var html, jsonItem, TrafficObj;
	var APIReverseGeocode = "https://developers.onemap.sg/privateapi/commonsvc/revgeocode";
	
	//Initialisation	
	html = "<h5>Traffic Cam Locations on " + searchDate + " " + searchTime + "</h5>";
	document.getElementById("panelLocationList").innerHTML += html;
		
	if( isJsonResponseEmpty(res.data.items) )
	{
		document.getElementById("panelLocationList").innerHTML += "Traffic Cam Not Found.";
	}
	else
	{
		//Traffic data is inside cameras object.
		jsonItem = res.data.items[0].cameras;
		jsonItem.forEach((item) => {
			
		  //Store the Traffic data inside object, which will be passed to another function onclick to show the details on the Traffic Image, Info, and weather forecast
		  var TrafficObj = new Object();
		  TrafficObj.camera_id = item.camera_id;
		  TrafficObj.latitude = item.location.latitude;
		  TrafficObj.longitude = item.location.longitude;
		  TrafficObj.imageURL = item.image;
		  TrafficObj.timestamp = item.timestamp;	  
		  var a = document.createElement('a'); 
		  a.setAttribute('href',"#wf");
		  a.setAttribute('class',"collection-item");
		  a.setAttribute('name',"col-list");
		  a.setAttribute('id',item.camera_id);
		  a.onclick = function () {loadImageAndWeatherForecast(this, TrafficObj);};
		  document.getElementById("panelLocationList").appendChild(a);
		  
		  //Invoke reverse geolocation and update LocationList
		  loadProgressBar();
		  axios.get(APIReverseGeocode, {
			params:{
				token: OneMapToken,
				location: item.location.latitude + "," + item.location.longitude
			}
		  })	
		  .then((response) => {	  
			updatePanelLocationList(response, item.camera_id);		  
		  })
		  .catch(error => {
			console.log(error)
		  });
		  
		});
	}
}

//Update the Location List from reverse geolocation to show as road name
function updatePanelLocationList(res, updatePanelID)
{
	//reverse geolocation might return empty
	var LocationRoad;
	
	if( isJsonResponseEmpty(res.data.GeocodeInfo) )
	{
		LocationRoad = updatePanelID;
	}
	else
	{
		LocationRoad = res.data.GeocodeInfo[0].ROAD;
	}
	
	document.getElementById(updatePanelID).innerHTML += LocationRoad;
}

//To load the Traffic Image and its info, together with retrieving Weather Forecast
function loadImageAndWeatherForecast(e, TrafficObj)
{
	var html;
	var friendlyDT = convertFriendlyDateTimeObj(TrafficObj.timestamp);
	
	updatePanelWeatherForecast(TrafficObj.latitude, TrafficObj.longitude);
		
	html = "<div class='card'>";
	html += "<div class='card-image'><img src='" + TrafficObj.imageURL + "'> <span class='card-title'>" + TrafficObj.camera_id + "</span></div>";
	html += "<div class='card-content' id='TrafficInfo'><p>Location: " + document.getElementById(TrafficObj.camera_id).innerHTML + " <br /> Captured at: " + friendlyDT.friendlyDateTime + "</p></div>";
	html += "</div>"	
	document.getElementById("panelScreenshot").innerHTML = html;
	
	//highlight user selection from the list
	var locationList = document.getElementsByName("col-list");
	for (var i = 0; i < locationList.length; i++) {
	   locationList[i].className = "collection-item";
	}
	e.className = "collection-item active";
}

//To refresh Weather Forecast panel
function updatePanelWeatherForecast(lat, lng)
{
	var html, jsonItem;
	var arrNearby = new Array();
	var arrNearbyWeather = new Array();
	var maxDistance = 1;
	var boolNotFound = true;
	var friendlyDTStart, friendlyDTEnd;
	
	//Initialisation
	html = "<ul class='collection with-header'>";
	html += "<li class='collection-header'><h5>Weather Forecast</h5>";	
	
	//can be blank too
	if( isJsonResponseEmpty(jsonWeatherForecast.data.area_metadata) || isJsonResponseEmpty(jsonWeatherForecast.data.items) )
	{		
		html += searchDate + " " + searchTime + "</li>";
		html += "<li class='collection-item'>"; 
		html += "<span class='title'>Weather forecast not found.</span></li>";
		html += "</ul>"	
		document.getElementById("panelWeatherForecast").innerHTML = html;
	}
	else
	{
		//keep on looping to find nearby area from the traffic cam
		while (boolNotFound)
		{			
			for (var i = 0; i < jsonWeatherForecast.data.area_metadata.length; i++) {
				if (distance(lat, lng, jsonWeatherForecast.data.area_metadata[i].label_location.latitude, jsonWeatherForecast.data.area_metadata[i].label_location.longitude, "K") <= maxDistance) {
					arrNearby.push(jsonWeatherForecast.data.area_metadata[i].name);
				}
			}
			if (arrNearby.length > 0)
			{
				boolNotFound = false;
			}
			maxDistance *= 2;		//if not found, double up the maxDistance
		}
		
		//Get its weather forecast
		friendlyDTStart = convertFriendlyDateTimeObj(jsonWeatherForecast.data.items[0].valid_period.start)
		friendlyDTEnd =  convertFriendlyDateTimeObj(jsonWeatherForecast.data.items[0].valid_period.end)
		html += friendlyDTStart.friendlyDateTime + " to " + friendlyDTEnd.friendlyDateTime + "</li>";
		for (let i = 0; i < jsonWeatherForecast.data.items[0].forecasts.length; i++) {
			for (let j = 0; j < arrNearby.length; j++) {
				if (jsonWeatherForecast.data.items[0].forecasts[i].area == arrNearby[j] ) {
					arrNearbyWeather.push(jsonWeatherForecast.data.items[0].forecasts[i].forecast);
				}
			}			
		}
		
		//Show on UI
		for (let i = 0; i < arrNearby.length; i++) {
			html += "<li class='collection-item'>"; //TODO get the weather forecast icon
			html += "<span class='title'>" + arrNearby[i] + "</span><p>" + arrNearbyWeather[i] + "</p></li>";			
		}
		html += "</ul>"	
		document.getElementById("panelWeatherForecast").innerHTML = html;
	}    
}

//To calculate the distance from 1 position to another. Source from https://www.geodatasource.com/developers/javascript
function distance(lat1, lon1, lat2, lon2, unit) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist;
	}
}

function convertFriendlyDateTimeObj(strDate)
{
	var dt, friendlyDate, friendlyTime, friendlyDateTime;
	dt = new Date(strDate);
	friendlyDate = dt.getFullYear() + "-" + (dt.getMonth() < 9 ? "0" + (dt.getMonth()+1) : dt.getMonth()+1) + "-" + (dt.getDate() < 10 ? "0" + (dt.getDate()) : dt.getDate());
	friendlyTime = dt.getHours() + ":" + dt.getMinutes();
	friendlyDateTime = friendlyDate + " " + friendlyTime
	
	return {
		friendlyDateTime,
        friendlyDate,
        friendlyTime
    };
	
}


function isJsonResponseEmpty(res)
{
	//check for []
	if( !$.isArray(res) ||  !res.length )
	{
		return true;
	}
	else
	{
		//check for [{}]
		for(var i in res[0]) 
		{
			return false;			
		}
		return true;
	}
	
}